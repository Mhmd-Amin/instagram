package sbu.cs.group4.frontEnd.View.Children;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import sbu.cs.group4.connectors.elements.Post;
import sbu.cs.group4.connectors.elements.Story;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class HomeController extends NotificationController implements Initializable
{
    @FXML
    private GridPane postGrid;
    @FXML
    private GridPane storyGrid;
    @FXML
    private Circle profileImage;

    @FXML
    void AddStory(ActionEvent event) throws IOException
    {
        switchScene(event, "AddStoryTab");
    }

    @FXML
    void DirectButton(ActionEvent event) throws IOException
    {
        switchScene(event, "searchTab");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        showNotificationSign();

        try
        {
            showPosts();
            showStories();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        //initialize a default profile picture
        Image image = new Image(getClass().getResource("/Image/tempProfile.PNG").toExternalForm());
        profileImage.setFill(new ImagePattern(image));
    }

    private void showPosts() throws IOException, InterruptedException
    {
        ArrayList<Post> homePosts;

        if((homePosts = serverHandler.getHomePosts(activeUser.getUsername())) != null)
        {
            if(serverHandler.getPostFiles(homePosts))
            {
                int row = 0;

                for (Post post : homePosts)
                {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(getClass().getResource("/fxml/OthersPostBox.fxml"));

                    VBox postBox = fxmlLoader.load();

                    PostBoxController postBoxController = fxmlLoader.getController();
                    postBoxController.setPost(post);

                    postGrid.add(postBox, 0, row++);
                }
            }

            else
            {
                System.out.println("could not initialize home tab");
            }
        }

        else
        {
            System.out.println("could not initialize home tab");
        }
    }

    private void showStories() throws InterruptedException, IOException
    {
        ArrayList<Story> homeStories;

        if((homeStories = serverHandler.getHomeStories(activeUser.getUsername())) != null)
        {
            if(serverHandler.getStoryFiles(homeStories))
            {
                int row = 0;

                for (Story story : homeStories)
                {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(getClass().getResource("/fxml/StoryBox.fxml"));

                    VBox storyBox = fxmlLoader.load();

                    StoryBoxController storyBoxController = fxmlLoader.getController();
                    storyBoxController.setStory(story);

                    storyGrid.add(storyBox, 0, row++);
                }
            }

            else
            {
                System.out.println("could not initialize home tab");
            }
        }

        else
        {
            System.out.println("could not initialize home tab");
        }
    }
}
