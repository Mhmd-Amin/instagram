package sbu.cs.group4.frontEnd.View.Children;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import sbu.cs.group4.connectors.elements.Story;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;

public class AddStoryController extends NotificationController
{
    @FXML
    private JFXTextField storyAddressField;

    @FXML
    void doneButton(ActionEvent event) throws IOException, InterruptedException
    {
        Story story;
        if ((story = serverHandler.story(storyAddressField.getText())) != null)
        {
            activeUser.addStory(story);
            switchScene(event, "profileTab(Posts)");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

}
