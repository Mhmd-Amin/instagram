package sbu.cs.group4.frontEnd.View.Parent;

import com.jfoenix.controls.JFXSnackbar;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sbu.cs.group4.connectors.elements.User;
import sbu.cs.group4.frontEnd.clientClasses.ServerHandler;

import java.io.IOException;

public abstract class ControllerClass
{
    protected static final String MEDIA_PATH = "src/main/java/sbu/cs/group4/frontEnd/media/";
    protected static ServerHandler serverHandler = null;
    protected static User activeUser = null;

    public static void setServerHandler(ServerHandler newServerHandler)
    {
        serverHandler = newServerHandler;
    }

    public static void setActiveUser(User newActiveUser)
    {
        activeUser = newActiveUser;
    }

    public void switchScene(ActionEvent event, String fxmlFileName) throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/" + fxmlFileName + ".fxml"));
        Stage s1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        s1.setScene(scene);
        s1.show();
    }

    public void returnToProfileTab(ActionEvent event) throws IOException
    {
        switchScene(event, "ProfileTab(Posts)");
    }
}