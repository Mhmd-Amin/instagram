package sbu.cs.group4.connectors.elements;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Post implements Comparable<Post>, Serializable
{
    private int postID;

    private String poster;
    private String caption;
    private Date postDate;

    private ArrayList<String> likes;
    private ArrayList<Comment> comments;


    public Post(int postID, String poster, String caption, Date postDate)
    {
        this.postID = postID;
        this.poster = poster;
        this.caption = caption;
        this.postDate = postDate;

        likes = new ArrayList<>();
        comments = new ArrayList<>();
    }

    public Post(String poster, String caption, Date postDate)
    {
        this.poster = poster;
        this.caption = caption;
        this.postDate = postDate;

        likes = new ArrayList<>();
        comments = new ArrayList<>();
    }


    public static Post postParser(ResultSet postResult) throws SQLException
    {
        int postID = postResult.getInt("postID");

        String poster = postResult.getString("poster");
        String caption = postResult.getString("caption");
        Date postDate = new Date(postResult.getLong("postDate"));

        return new Post(postID, poster, caption, postDate);
    }

    public void like(String liker)
    {
        likes.add(liker);
    }

    public void unlike(String unLiker)
    {
        likes.remove(unLiker);
    }

    public void addComment(Comment comment)
    {
        comments.add(comment);
    }

    public void removeComment(Comment comment)
    {
        comments.remove(comment);
    }

    public ArrayList<String> getCommentsAsString()
    {
        Collections.sort(comments);

        ArrayList<String> commentString = new ArrayList<>();

        for (Comment comment : comments)
        {
            commentString.add(comment.getCommenter() + " :\n" + comment.getCommentText());
        }

        return commentString;
    }

    //setter

    public void setPostID(int postID)
    {
        this.postID = postID;
    }

    public void setPoster(String poster)
    {
        this.poster = poster;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    public void setPostDate(Date postDate)
    {
        this.postDate = postDate;
    }

    public void setLikes(ArrayList<String> likes)
    {
        this.likes = likes;
    }

    public void setComments(ArrayList<Comment> comments)
    {
        this.comments = comments;
    }


    //getter


    public int getPostID()
    {
        return postID;
    }

    public String getPoster()
    {
        return poster;
    }

    public String getCaption()
    {
        return caption;
    }

    public Date getPostDate()
    {
        return postDate;
    }

    public ArrayList<String> getLikes()
    {
        return likes;
    }

    public ArrayList<Comment> getComments()
    {
        Collections.sort(comments);

        return comments;
    }

    @Override
    public int compareTo(Post o)
    {
        return this.postDate.compareTo(o.getPostDate());
    }
}
