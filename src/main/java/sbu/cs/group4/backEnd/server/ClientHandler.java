package sbu.cs.group4.backEnd.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import sbu.cs.group4.backEnd.database.SqlHandler;
import sbu.cs.group4.connectors.File.FileHandler;
import sbu.cs.group4.connectors.dataTransfer.DataTransferProcessor;
import sbu.cs.group4.connectors.elements.*;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class ClientHandler implements Runnable
{
    private Server server;

    private SqlHandler sqlHandler;
    private Gson gson;
    private DataTransferProcessor dtp;
    private FileHandler fileHandler;

    private JsonObject jsonInput;
    private JsonObject jsonOutput;

    private User activeUser;

    private final String MEDIA_PATH = "src/main/java/sbu/cs/group4/backEnd/media/";


    public ClientHandler(Socket socket, Server server) throws IOException
    {
        this.server = server;

        sqlHandler = server.getSqlHandler();
        gson = new GsonBuilder().serializeNulls().create();
        dtp = new DataTransferProcessor(socket);
        fileHandler = new FileHandler();

        jsonOutput = null;
        jsonInput = null;

        activeUser = null;
    }


    public String getUsername()
    {
        return activeUser.getUsername();
    }

    public DataTransferProcessor getDtp()
    {
        return dtp;
    }

    @Override
    public void run()
    {
        try
        {
            while ((jsonInput = dtp.receiveJson()) != null)
            {
                System.out.println(jsonInput);

                switch (jsonInput.get("request").getAsString().trim().toLowerCase())
                {
                    case "sign up":
                        signUp();
                        break;

                    case "log in":
                        logIn();
                        break;

                    case "get file":
                        getFile();
                        break;

                    case "get user":
                        getUser();
                        break;

                    case "update user":
                        updateUser();
                        break;


                    case "get home posts":
                        getHomePosts();
                        break;

                    case "get home stories":
                        getHomeStories();
                        break;

                    case "post":
                        post();
                        break;

                    case "delete post":
                        deletePost();
                        break;


                    case "story":
                        story();
                        break;

                    case "delete story":
                        deleteStory();
                        break;


                    case "send message":
                        sendMessage();
                        break;

                    case "delete message":
                        deleteMessage();
                        break;


                    case "comment":
                        comment();
                        break;

                    case "delete comment":
                        deleteComment();
                        break;


                    case "like":
                        like();
                        break;

                    case "unlike":
                        unlike();
                        break;


                    case "follow":
                        follow();
                        break;

                    case "unfollow":
                        unfollow();
                        break;


                    case "delete user":
                        deleteUser();
                        break;

                    case "logout":
                        logout();
                        break;
                }
            }
        }

        catch (SQLException | InterruptedException | IOException e)
        {
            e.printStackTrace();

            jsonOutput = new JsonObject();
            jsonOutput.addProperty("result", false);

            dtp.sendJson(jsonOutput);
        }
    }

    private void signUp() throws SQLException, IOException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the user information
        String username = jsonInput.get("username").getAsString();
        String password = jsonInput.get("password").getAsString();
        String fullName = jsonInput.get("fullName").getAsString();
        String email = jsonInput.get("email").getAsString();

        //the username is taken
        if (sqlHandler.userExists(username))
        {
            //fill the jsonOutput
            jsonOutput.addProperty("result", false);
            jsonOutput.addProperty("error",
                    "the username " + username + " is already taken.");
        }

        //the username is available
        else
        {
            //initialize the activeUser
            activeUser = new User(username, password, fullName, email, new Date());

            //add the activeUser to the database
            sqlHandler.addUser(activeUser);

            //making directories
            fileHandler.makeNecessaryDirectory(MEDIA_PATH, username);

            //fill the jsonOutput
            addUserToOutput(activeUser);
        }

        System.out.println(jsonOutput);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void logIn() throws SQLException, IOException, InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the user information
        String providedUsername = jsonInput.get("username").getAsString();
        String providedPassword = jsonInput.get("password").getAsString();

        //get the user from the database
        User user = sqlHandler.getUser(providedUsername, providedPassword);

        //user not found
        if (user == null)
        {
            //fill the jsonOutput
            jsonOutput.addProperty("result", false);
            jsonOutput.addProperty("error",
                    "wrong username or password.");
        }

        //user found
        else
        {
            //initialize the activeUser
            activeUser = user;

            //fill the jsonOutput
            addUserToOutput(activeUser);
        }

        System.out.println(jsonOutput);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void getFile() throws IOException
    {
        //initialize the fileOutput
        jsonOutput = new JsonObject();

        //get the username
        String username = jsonInput.get("username").getAsString();

        //get the filePath

        String filePath = MEDIA_PATH + username + "/";

        switch (jsonInput.get("fileType").getAsString())
        {
            case "profile":
                filePath = filePath + username + ".png";
                break;

            case "post":
                filePath = filePath + "posts/" + jsonInput.get("postID").getAsInt() + ".png";
                break;

            case "story":
                filePath = filePath + "stories/" + jsonInput.get("storyID").getAsInt() + ".png";
                break;
        }

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("file", fileHandler.encodeFile(new File(filePath)));

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void getUser() throws SQLException, IOException, InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the user information
        String username = jsonInput.get("username").getAsString();

        //get the user from the database
        User user = sqlHandler.getUser(username);

        //user not found
        if (user == null)
        {
            //fill the jsonOutput
            jsonOutput.addProperty("result", false);
            jsonOutput.addProperty("error", "user not found.");
        }

        //user found
        else
        {
            //fill the jsonOutput
            addUserToOutput(user);
        }

        System.out.println(jsonOutput);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void updateUser() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the user's updated information
        String email = jsonInput.get("email").getAsString();
        String password = jsonInput.get("password").getAsString();
        String fullName = jsonInput.get("fullName").getAsString();
        String bio = jsonInput.get("bio").getAsString();

        //update the activeUser
        activeUser.setEmail(email);
        activeUser.setPassword(password);
        activeUser.setBio(bio);
        activeUser.setFullName(fullName);

        //update the activeUser in the database
        sqlHandler.updateUser(activeUser);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        System.out.println(jsonOutput);

        //send the jsonOutput to the database
        dtp.sendJson(jsonOutput);
    }

    private void getHomePosts() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //initialize the posts of the timeLine
        ArrayList<Post> homePosts = new ArrayList<>();

        //fill the list
        for (String following : activeUser.getFollowings())
        {
            ArrayList<Post> postList;

            postList = sqlHandler.getAllPosts(following);

            if (!postList.isEmpty())
            {
                homePosts.addAll(postList);
            }
        }

        Collections.sort(homePosts);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("home posts", gson.toJson(homePosts));

        System.out.println(jsonOutput);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void getHomeStories() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //initialize the stories of the timeLine
        ArrayList<Story> homeStories = new ArrayList<>();

        //fill the list
        for (String following : activeUser.getFollowings())
        {
            ArrayList<Story> storyList;

            storyList = sqlHandler.getAllStories(following);

            if (!storyList.isEmpty())
            {
                homeStories.addAll(storyList);
            }
        }

        Collections.sort(homeStories);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("home stories", gson.toJson(homeStories));

        System.out.println(jsonOutput);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void post() throws SQLException, IOException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the post's information
        String poster = activeUser.getUsername();
        String caption = jsonInput.get("caption").getAsString();
        String encodedFile = jsonInput.get("file").getAsString();
        Date postDate = new Date();

        //make a new post instance
        Post post = new Post(poster, caption, postDate);

        //add the post to the database
        int postID = sqlHandler.addPost(post);

        //complete the post instance
        post.setPostID(postID);

        //add the post to the activeUser
        activeUser.addPost(post);

        //save the file of the post
        fileHandler.decodeFile(encodedFile,
                MEDIA_PATH + activeUser.getUsername() + "/posts/" + postID + ".png");

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("post", gson.toJson(post));

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    public void deletePost() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the postId
        int postID = jsonInput.get("postID").getAsInt();

        //delete the post from the activeUser's posts
        activeUser.deletePost(postID);

        //delete the post from the database
        sqlHandler.deletePost(postID);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    private void like() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //initialize the jsonNotification
        JsonObject jsonNotification = new JsonObject();

        //get the like's information
        int postID = jsonInput.get("postID").getAsInt();
        String poster = jsonInput.get("poster").getAsString();
        String liker = activeUser.getUsername();

        //add the like to the database
        sqlHandler.addLike(postID, liker);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //fill the jsonNotification
        jsonNotification.addProperty("notification", "like");
        jsonNotification.addProperty("notificationText",
                liker + " has just liked one of your posts!");

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);

        //send a notification to the poster
        server.sendNotification(poster, jsonNotification);
    }

    public void unlike() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the unlike's information
        int postID = jsonInput.get("postID").getAsInt();
        String unliker = activeUser.getUsername();

        //delete the like from the database
        sqlHandler.unlike(postID, unliker);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    public void comment() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the comment's information
        int postID = jsonInput.get("postID").getAsInt();
        String commentText = jsonInput.get("commentText").getAsString();
        String commenter = activeUser.getUsername();

        //make a new comment instance
        Comment comment = new Comment(postID, commenter, commentText, new Date());

        //add the comment to the database
        int commentID = sqlHandler.addComment(comment);

        //complete the comment instance
        comment.setID(commentID);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("comment", gson.toJson(comment));

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    private void deleteComment() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the commentId
        int commentID = jsonInput.get("commentID").getAsInt();

        //delete the comment from the database
        sqlHandler.deleteComment(commentID);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    private void follow() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //initialize the notificationJson
        JsonObject notificationJson = new JsonObject();

        //get the follow's information
        String followed = jsonInput.get("followed").getAsString();
        String follower = activeUser.getUsername();

        //add the follow to the database
        sqlHandler.addFollow(follower, followed);

        //add the follow to the activeUser
        activeUser.follow(followed);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //fill the notificationJson
        notificationJson.addProperty("notification", "follow");
        notificationJson.addProperty("notificationText",
                follower + " has just started following you!");

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);

        //send a notification to the followed user
        server.sendNotification(followed, notificationJson);
    }

    public void unfollow() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the unfollowed username
        String unfollowed = jsonInput.get("unfollowed").getAsString();
        String unfollower = activeUser.getUsername();

        //delete the follow from the database
        sqlHandler.deleteFollow(unfollower, unfollowed);

        //delete the follow from the activeUser
        activeUser.unfollow(unfollowed);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    private void sendMessage() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //initialize the jsonNotification
        JsonObject jsonNotification = new JsonObject();

        //get the message's information
        String messageSender = activeUser.getUsername();
        String messageReceiver = jsonInput.get("messageReceiver").getAsString();
        String messageText = jsonInput.get("messageText").getAsString();
        Date messageDate = new Date();

        //make a new message instance
        Message message = new Message(messageSender, messageReceiver, messageText, messageDate);

        //add the message to the database
        int messageID = sqlHandler.addMessage(message);

        //complete the message instance
        message.setMessageID(messageID);

        //add the message to the activeUser
        activeUser.addMessage(message);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("message", gson.toJson(message));

        //fill the jsonNotification
        jsonNotification.addProperty("notification", "message");
        jsonNotification.addProperty("notificationText",
                messageSender + " has just sent you a message!");

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);

        //send a notification to the messageReceiver
        server.sendNotification(messageReceiver, jsonNotification);
    }

    public void deleteMessage() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the messageId
        int messageID = jsonInput.get("messageID").getAsInt();

        //delete the message from the database
        sqlHandler.deleteMessage(messageID);

        //delete the message from the activeUser's messages
        activeUser.deleteMessage(messageID);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    public void story() throws SQLException, IOException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the story's information
        String poster = activeUser.getUsername();
        Date storyDate = new Date();

        //make a new story instance
        Story story = new Story(poster, storyDate);

        //add the story to the database
        int storyID = sqlHandler.addStory(story);

        //complete the story instance
        story.setStoryID(storyID);

        //add the story to the activeUser
        activeUser.addStory(story);

        //save the file of the story
        fileHandler.decodeFile(jsonInput.get("file").getAsString(),
                MEDIA_PATH + getUsername() + "/stories/" + storyID + ".png");

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("story", gson.toJson(story));

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    public void deleteStory() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the story id
        int storyID = jsonInput.get("storyID").getAsInt();

        //delete the story from the database
        sqlHandler.deleteStory(storyID);

        //delete the story from the activeUser
        activeUser.deleteStory(storyID);

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
        System.out.println(jsonOutput);
    }

    private void deleteUser() throws SQLException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //get the username of the deleted user
        String username = activeUser.getUsername();

        //delete the user from the database
        sqlHandler.deleteUser(username);

        //deleting files
        fileHandler.deleteDirectory(MEDIA_PATH + getUsername());

        //fill the jsonOutput
        jsonOutput.addProperty("result", true);

        System.out.println(jsonOutput);

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    private void logout() throws IOException
    {
        activeUser = null;
        server.removeClient(this);
    }

    private void addUserToOutput(User user)
    {
        String userJson = gson.toJson(user);
        jsonOutput.addProperty("result", true);
        jsonOutput.addProperty("user", userJson);
    }
}