package sbu.cs.group4.frontEnd.View.Children;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.Story;
import sbu.cs.group4.connectors.elements.User;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;

import java.io.IOException;

public class StoryBoxController extends ControllerClass
{
    private static Story story;

    @FXML
    private static Text UsernameText;

    @FXML
    private static Circle StoryCircle;

    public static void setStory(Story newStory)
    {
        story = newStory;

        UsernameText.setText(story.getStoryPoster());

        Image image = new Image(StoryBoxController.class.getResourceAsStream
                (MEDIA_PATH + story.getStoryPoster() + "/stories/" + story.getStoryID() + ".png"));
        StoryCircle.setFill(new ImagePattern(image));
    }

    @FXML
    void StoryCircle(ActionEvent event) throws IOException, InterruptedException
    {
        User poster;

        if((poster = serverHandler.getUser(story.getStoryPoster())) != null)
        {
            DirectController.setMessageReceiver(poster);
            switchScene(event, "WriteDirectTab");
        }

        else
        {
            returnToProfileTab(event);
        }
    }
}
