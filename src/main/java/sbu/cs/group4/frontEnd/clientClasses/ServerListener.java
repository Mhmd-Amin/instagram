package sbu.cs.group4.frontEnd.clientClasses;

import com.google.gson.JsonObject;
import sbu.cs.group4.connectors.dataTransfer.DataTransferProcessor;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;

public class ServerListener implements Runnable
{
    private ServerHandler serverHandler;
    private DataTransferProcessor dtp;
    private JsonObject input;


    public ServerListener(ServerHandler serverHandler)
    {
        this.serverHandler = serverHandler;
        dtp = serverHandler.getDtp();
        input = null;
    }

    @Override
    public void run()
    {
        try
        {
            //receive json messages from server
            while ((input = dtp.receiveJson()) != null)
            {
                //the received json message is a notification
                if (input.has("notification"))
                {
                    //add the notification to the list of new notifications
                    NotificationController.addNotification(input);
                }

                //the received json message is a response to a request of the client
                else if (input.has("result"))
                {
                    //assign the jsonInput to the serverHandler's jsonInput variable
                    serverHandler.setJsonInput(input);
                }
            }
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
