package sbu.cs.group4.frontEnd.View.Children;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.Post;
import sbu.cs.group4.connectors.elements.User;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewUserController extends NotificationController
{
    private static User user;

    @FXML
    private GridPane postGrid;
    @FXML
    private Circle profileImage;
    @FXML
    private Text postsNumber;
    @FXML
    private Text followersNumber;
    @FXML
    private Text followingsNumber;
    @FXML
    private Text usernameField;
    @FXML
    private Text Biography;

    public static void setUser(User newUser)
    {
        user = newUser;
    }


    @FXML
    void backButton(ActionEvent event) throws IOException
    {
        switchScene(event, "SearchTab");
    }

    @FXML
    void messageButton(ActionEvent event) throws IOException
    {
        DirectController.setMessageReceiver(user);
        switchScene(event, "WriteDirectTab");
    }

    @FXML
    void unfollowButton(ActionEvent event) throws IOException, InterruptedException
    {
        if (serverHandler.unfollow(user.getUsername()))
        {
            user.removeFollower(activeUser.getUsername());
            switchScene(event, "followUserTab");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

    @FXML
    void followButton(ActionEvent event) throws IOException, InterruptedException
    {
        if (serverHandler.follow(user.getUsername()))
        {
            user.addFollower(activeUser.getUsername());
            switchScene(event, "UnfollowUserTab");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

    @FXML
    void IGTVButton(ActionEvent event)
    {
    }

    @FXML
    void TaggedPostButton(ActionEvent event)
    {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //show the notifications

        showNotificationSign();

        //initialize the list of posts
        try
        {
            showPosts();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        //initialize a default profile picture

        Image image = new Image(getClass().getResource("/Image/tempProfile.PNG").toExternalForm());
        profileImage.setFill(new ImagePattern(image));

        //fill stuff

        usernameField.setText(user.getUsername());
        Biography.setText(user.getBio());
        followersNumber.setText(String.valueOf(user.getFollowers().size()));
        followingsNumber.setText(String.valueOf(user.getFollowings().size()));
        postsNumber.setText(String.valueOf(user.getPosts().size()));
    }

    private void showPosts() throws IOException, InterruptedException
    {
        ArrayList<Post> posts = user.getPosts();

        if (serverHandler.getPostFiles(posts))
        {
            int row = 0;

            for (Post post : posts)
            {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/fxml/OthersPostBox.fxml"));

                VBox postBox = fxmlLoader.load();

                PostBoxController postBoxController = fxmlLoader.getController();
                postBoxController.setPost(post);

                postGrid.add(postBox, 0, row++);
            }
        }

        else
        {
            System.out.println("could not initialize posts");
        }
    }
}
