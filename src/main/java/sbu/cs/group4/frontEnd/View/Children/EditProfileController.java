package sbu.cs.group4.frontEnd.View.Children;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EditProfileController extends ControllerClass implements Initializable
{
    @FXML
    private Circle ProfileCircle;
    @FXML
    private TextField nameField;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField bioField;


    @FXML
    void CancelButton(ActionEvent event) throws IOException
    {
        switchScene(event, "ProfileTab(Posts)");
    }

    @FXML
    void DoneButton(ActionEvent event) throws IOException, InterruptedException
    {
        String fullName = nameField.getText();
        String email = emailField.getText();
        String password = passwordField.getText();
        String bio = bioField.getText();
        if ((activeUser = serverHandler.updateUser(activeUser, fullName, password, email, bio)) != null)
        {
            switchScene(event, "ProfileTab(Posts)");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //initialize a default profile picture

        Image image = new Image(getClass().getResource("/Image/tempProfile.PNG").toExternalForm());
        ProfileCircle.setFill(new ImagePattern(image));

        //set the current values to the fields
        nameField.setText(activeUser.getFullName());
        passwordField.setText(activeUser.getPassword());
        bioField.setText(activeUser.getBio());
        emailField.setText(activeUser.getEmail());
    }
}
