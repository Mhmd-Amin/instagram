package sbu.cs.group4.connectors.dataTransfer;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;

public class DataTransferProcessor
{
    private Socket socket;

    //writer and reader
    private PrintWriter writer;
    private BufferedReader reader;

    private Gson gson;
    private Base64.Encoder encoder;
    private Base64.Decoder decoder;

    public DataTransferProcessor(Socket socket) throws IOException
    {
        this.socket = socket;

        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new PrintWriter(socket.getOutputStream(), true);

        gson = new GsonBuilder().serializeNulls().create();
        encoder = Base64.getEncoder();
        decoder = Base64.getDecoder();
    }

    public void sendJson(JsonObject jsonObj)
    {
        String jsonStr = gson.toJson(jsonObj);
        String encodedJson = encoder.encodeToString(jsonStr.getBytes());
        writer.println(encodedJson);
    }

    public JsonObject receiveJson() throws IOException
    {
        String encodedStr = reader.readLine();

        if (encodedStr == null)
        {
            return null;
        }

        String decodedStr = new String(decoder.decode(encodedStr));
        JsonElement jsonElement = new JsonParser().parse(decodedStr);

        if (jsonElement.isJsonObject())
        {
            return jsonElement.getAsJsonObject();
        }
        return null;
    }

    public void close() throws IOException
    {
        writer.close();
        reader.close();
    }
}