package sbu.cs.group4.frontEnd.View.Children;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;

import java.io.IOException;

public class LoginController extends ControllerClass
{
    @FXML
    private JFXTextField usernameField;
    @FXML
    private JFXPasswordField passwordField;
    @FXML
    private Text textField;


    @FXML
    void loginButton(ActionEvent event) throws IOException, InterruptedException
    {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if((activeUser = serverHandler.login(username, password)) != null)
        {
            if(serverHandler.getPostFiles(activeUser.getPosts()))
            {
                if(serverHandler.getStoryFiles(activeUser.getStories()))
                {
                    //go to profile tab panel
                    switchScene(event, "ProfileTab(Posts)");
                }

                else
                {
                    textField.setText("something went wrong");
                }
            }

            else
            {
                textField.setText("something went wrong");
            }
        }

        else
        {
            textField.setText("wrong username or password");
        }
    }

    @FXML
    void signupClick(ActionEvent event) throws IOException
    {
        switchScene(event, "Signup");
    }
}



