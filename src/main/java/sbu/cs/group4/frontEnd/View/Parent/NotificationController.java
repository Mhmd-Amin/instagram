package sbu.cs.group4.frontEnd.View.Parent;

import com.google.gson.JsonObject;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public abstract class NotificationController extends ControllerClass implements Initializable
{
    private static ArrayList<JsonObject> notifications = new ArrayList<>();

    private static boolean notificationAvailable = false;

    @FXML
    protected ImageView NotificationSign;


    public static ArrayList<JsonObject> getNotifications()
    {
        return notifications;
    }

    public static boolean isNotificationAvailable()
    {
        return notificationAvailable;
    }

    public static void addNotification(JsonObject notification)
    {
        notifications.add(notification);
        notificationAvailable = true;
    }

    public static void clearNotifications()
    {
        notifications.clear();
        notificationAvailable = false;
    }

    public void showNotificationSign()
    {
        //a notification is available
        if(notificationAvailable)
        {
            JsonObject lastNotification = notifications.get(notifications.size() - 1);
            String lastNotificationType = lastNotification.get("notification").getAsString();

            switch (lastNotificationType)
            {
                case "message":
                    NotificationSign.setImage(new Image("Image/haveDirect.PNG"));

                case "follow":
                    NotificationSign.setImage(new Image("Image/haveFollower.PNG"));

                case "like":
                    NotificationSign.setImage(new Image("Image/haveLike.PNG"));

                case "comment":
                    NotificationSign.setImage(new Image("Image/commentNotification.PNG"));
            }
        }
    }

    @FXML
    void homeButton(ActionEvent event) throws IOException
    {
        switchScene(event, "HomeTab");
    }

    @FXML
    void searchButton(ActionEvent event) throws IOException
    {
        switchScene(event, "SearchTab");
    }

    @FXML
    void addPostButton(ActionEvent event) throws IOException
    {
        switchScene(event, "AddPostTab");
    }

    @FXML
    void activityButton(ActionEvent event) throws IOException
    {
        switchScene(event, "ActivityTab");
    }

    @FXML
    void ProfileButton(ActionEvent event) throws IOException
    {
        switchScene(event, "ProfileTab(Posts)");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        showNotificationSign();
    }
}
