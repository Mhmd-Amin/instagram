package sbu.cs.group4.frontEnd.View.Children;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.Post;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ProfileController extends NotificationController
{
    @FXML
    private GridPane postGrid;
    @FXML
    private Circle profileImage;
    @FXML
    private Text postsNumber;
    @FXML
    private Text followersNumber;
    @FXML
    private Text followingsNumber;
    @FXML
    private Text UsernameText;
    @FXML
    private Text Biography;

    @FXML
    void postButton(ActionEvent event) throws IOException
    {
        switchScene(event, "ProfileTab(Posts)");
    }

    @FXML
    void addPost(ActionEvent event) throws IOException
    {
        switchScene(event, "AddPostTab");
    }

    @FXML
    void editProfile(ActionEvent event) throws IOException
    {
        switchScene(event, "EditProfileTab");
    }

    @FXML
    void setting(ActionEvent event) throws IOException
    {
        switchScene(event, "SettingTab");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        showNotificationSign();

        ArrayList<Post> posts = activeUser.getPosts();

        int row = 0;
        try
        {
            for (Post post : posts)
            {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/fxml/MyPostBox.fxml"));

                VBox postBox = fxmlLoader.load();

                PostBoxController postBoxController = fxmlLoader.getController();
                postBoxController.setPost(post);

                postGrid.add(postBox, 0, row++);
            }
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }

        //initialize a default profile picture
        Image image = new Image(getClass().getResource("/Image/tempProfile.PNG").toExternalForm());
        profileImage.setFill(new ImagePattern(image));

        //fill stuff
        UsernameText.setText(activeUser.getUsername());
        Biography.setText(activeUser.getBio());
        followersNumber.setText(String.valueOf(activeUser.getFollowers().size()));
        followingsNumber.setText(String.valueOf(activeUser.getFollowings().size()));
        postsNumber.setText(String.valueOf(activeUser.getPosts().size()));
    }
}
