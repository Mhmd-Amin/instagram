package sbu.cs.group4.frontEnd.View.Children;

import com.jfoenix.controls.JFXSnackbar;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.Post;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PostBoxController extends ControllerClass implements Initializable
{
    private Post post;

    @FXML
    private Circle ProfileImage;
    @FXML
    private Text DateText;
    @FXML
    private ImageView PostImage;
    @FXML
    private Button likeButton;
    @FXML
    private Text LikeNumber;
    @FXML
    private Text commentNumber;
    @FXML
    private TextField CaptionField;


    public void setPost(Post post)
    {
        this.post = post;

        Image image = new Image(new File(MEDIA_PATH + post.getPoster()
                + "/posts/" + post.getPostID() + ".png").toURI().toString());
        PostImage.setImage(image);

        DateText.setText(String.valueOf(post.getPostDate()));
        LikeNumber.setText(String.valueOf(post.getLikes().size()));
        commentNumber.setText(String.valueOf(post.getComments().size()));
        CaptionField.setText(post.getCaption());
    }



    @FXML
    void commentButton(ActionEvent event) throws IOException
    {
        WriteCommentController.setPost(post);
        switchScene(event, "WriteComment");
    }

    @FXML
    void likeButton(ActionEvent event) throws IOException, InterruptedException
    {
        Image image = new Image(getClass().getResourceAsStream("/Image/activity1.png"));

        if (!post.getLikes().contains(activeUser.getUsername()))
        {
            if (serverHandler.like(post))
            {
                post.like(activeUser.getUsername());
                image = new Image(getClass().getResourceAsStream("/Image/like.png"));
            }

            else
            {
                returnToProfileTab(event);
            }
        }

        else
        {
            if (serverHandler.unlike(post))
            {
                post.unlike(activeUser.getUsername());
                image = new Image(getClass().getResourceAsStream("/Image/activity1.png"));
            }

            else
            {
                returnToProfileTab(event);
            }
        }


        Image finalImage = image;
        likeButton.
                setOnAction(
                        new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                Button button = (Button) e.getSource();
                button.setGraphic(new ImageView(finalImage));
            }
        });

        if(post.getPoster().equals(activeUser.getUsername()))
        {
            switchScene(event, "ProfileTab(Posts)");
        }
    }

    @FXML
    void likeButtonDragged(ActionEvent event) throws IOException
    {
        //make the list unique
        JFXSnackbar snack = new JFXSnackbar();
        snack.show(post.getLikes().toString(), 5000);
        snack.prefHeight(320.0);
        snack.prefWidth(320.0);
    }

    @FXML
    void deleteButton(ActionEvent event) throws IOException, InterruptedException
    {
        if(serverHandler.deletePost(post))
        {
            activeUser.deletePost(post.getPostID());
            switchScene(event, "ProfileTab(Posts)");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

    @FXML
    void SaveButton(ActionEvent event)
    {
    }

    @FXML
    void shareButton(ActionEvent event)
    {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        Image image = new Image(getClass().getResource("/Image/tempProfile.PNG").toExternalForm());
        ProfileImage.setFill(new ImagePattern(image));
    }
}

