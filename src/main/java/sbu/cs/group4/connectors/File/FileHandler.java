package sbu.cs.group4.connectors.File;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class FileHandler {
    private Base64.Encoder encoder;
    private Base64.Decoder decoder;

    public FileHandler()
    {
        encoder = Base64.getEncoder();
        decoder = Base64.getDecoder();
    }

    public void makeDirectory(String filePath)
    {
        File mkDir = new File(filePath);
        mkDir.mkdirs();
    }

    public static void createDirectory(String filePath)
    {
        File mkDir = new File(filePath);
        mkDir.mkdirs();
    }

    public void deleteDirectory(String directoryPath)
    {
        File directory = new File(directoryPath);
        if (directory.isDirectory())
        {
            File[] subDirectories = directory.listFiles();
            for (File subDirectory : subDirectories) {
                deleteDirectory(subDirectory.getPath());
            }
        }
        directory.delete();
        return;
    }

    public File fileFinder(String filePath)
    {
        File photoFile = new File(filePath);
        if (photoFile.exists())
        {
            return photoFile;
        }
        return null;
    }

    public void makeNecessaryDirectory(String MEDIA_PATH, String username)
    {
        makeDirectory(MEDIA_PATH + username + "/");
        makeDirectory(MEDIA_PATH + username + "/posts/");
        makeDirectory(MEDIA_PATH + username + "/stories/");
    }

    public String encodeFile(File file) throws IOException
    {
        byte[] fileBytes = Files.readAllBytes(file.toPath());

        return encoder.encodeToString(fileBytes);
    }

    public File decodeFile(String encodedFile, String savePath) throws IOException
    {
        byte[] decodedBytes = decoder.decode(encodedFile);

        try (FileOutputStream fos = new FileOutputStream(savePath))
        {
            fos.write(decodedBytes);
        }

        return new File(savePath);
    }
}
