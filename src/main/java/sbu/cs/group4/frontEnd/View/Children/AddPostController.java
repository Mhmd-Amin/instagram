package sbu.cs.group4.frontEnd.View.Children;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import sbu.cs.group4.connectors.elements.Post;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;

public class AddPostController extends NotificationController
{
    @FXML
    private JFXTextField postAddressField;
    @FXML
    private JFXTextField CaptionField;

    @FXML
    void doneButton(ActionEvent event) throws IOException, InterruptedException
    {
        Post post;
        if ((post = serverHandler.post(postAddressField.getText(), CaptionField.getText())) != null)
        {
            activeUser.addPost(post);
            switchScene(event, "profileTab(Posts)");
        }

        else
        {
            returnToProfileTab(event);
        }
    }
}