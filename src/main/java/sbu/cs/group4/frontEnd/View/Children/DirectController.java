package sbu.cs.group4.frontEnd.View.Children;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.Message;
import sbu.cs.group4.connectors.elements.User;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DirectController extends ControllerClass implements Initializable
{
    private static User messageReceiver;

    @FXML
    private ListView<String> MessageList;
    @FXML
    private JFXTextField typeMessageField;
    @FXML
    private Circle profileImage;
    @FXML
    private Text UsernameField;

    public static void setMessageReceiver(User newMessageReceiver)
    {
        messageReceiver = newMessageReceiver;
    }

    @FXML
    void BackButton(ActionEvent event) throws IOException
    {
        switchScene(event, "HomeTab");
    }

    @FXML
    void SendButton(ActionEvent event) throws IOException, InterruptedException
    {
        Message message;
        if ((message = serverHandler.message(messageReceiver.getUsername(), typeMessageField.getText())) != null)
        {
            activeUser.addMessage(message);
            switchScene(event, "WriteDirectTab");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

    //set profile image and username
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //initialize the profile picture

        Image image = new Image(getClass().getResource("/Image/tempProfile.PNG").toExternalForm());
        profileImage.setFill(new ImagePattern(image));

        //set the usernameField
        UsernameField.setText(messageReceiver.getUsername());

        //show the messages
        ArrayList<String> messageString = activeUser.getMessagesWithUserAsString(messageReceiver.getUsername());
        MessageList.setItems(FXCollections.observableList(messageString));
    }
}
