package sbu.cs.group4.frontEnd.clientClasses;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import sbu.cs.group4.connectors.File.FileHandler;
import sbu.cs.group4.connectors.dataTransfer.DataTransferProcessor;
import sbu.cs.group4.connectors.elements.*;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ServerHandler
{
    private JsonObject jsonInput;
    private JsonObject jsonOutput;

    private DataTransferProcessor dtp;
    private FileHandler fileHandler;
    private Gson gson;

    private final String MEDIA_PATH = "src/main/java/sbu/cs/group4/frontEnd/media/";

    public ServerHandler()
    {
        jsonInput = null;
        jsonOutput = null;

        dtp = null;
        fileHandler = new FileHandler();
        gson = new GsonBuilder().serializeNulls().create();
    }

    public ServerHandler(DataTransferProcessor dtp)
    {
        jsonInput = null;
        jsonOutput = null;

        this.dtp = dtp;
        fileHandler = new FileHandler();
        gson = new GsonBuilder().serializeNulls().create();
    }


    public DataTransferProcessor getDtp()
    {
        return dtp;
    }

    public void setJsonInput(JsonObject jsonInput)
    {
        this.jsonInput = jsonInput;
    }

    public boolean checkJsonResult()
    {
        if (jsonInput.has("result"))
        {
            return jsonInput.get("result").getAsBoolean();
        }

        return false;
    }

    /*requests*/

    //user

    public User signup(String username, String password, String email, String fullName) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //add the information to the jsonOutput
        jsonOutput.addProperty("request", "sign up");
        jsonOutput.addProperty("username", username);
        jsonOutput.addProperty("password", password);
        jsonOutput.addProperty("fullName", fullName);
        jsonOutput.addProperty("email", email);

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            fileHandler.makeNecessaryDirectory(MEDIA_PATH, username);

            User user = gson.fromJson(jsonInput.get("user").getAsString(), User.class);

            jsonInput = null;

            return user;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public User login(String username, String password) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //add the information to the jsonOutput
        jsonOutput.addProperty("request", "log in");
        jsonOutput.addProperty("username", username);
        jsonOutput.addProperty("password", password);

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            fileHandler.makeNecessaryDirectory(MEDIA_PATH, username);

            User user = gson.fromJson(jsonInput.get("user").getAsString(), User.class);

            jsonInput = null;

            return user;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean getPostFiles(ArrayList<Post> posts) throws InterruptedException, IOException
    {
        for (Post post : posts)
        {
            jsonOutput = new JsonObject();

            jsonOutput.addProperty("request", "get file");
            jsonOutput.addProperty("fileType", "post");
            jsonOutput.addProperty("username", post.getPoster());
            jsonOutput.addProperty("postID", post.getPostID());

            dtp.sendJson(jsonOutput);

            while (jsonInput == null)
            {
                Thread.sleep(100);
            }

            if (checkJsonResult())
            {
                fileHandler.decodeFile(jsonInput.get("file").getAsString(),
                        MEDIA_PATH + post.getPoster() + "/posts/" + post.getPostID() + ".png");

                jsonInput = null;
            }

            else
            {
                jsonInput = null;

                return false;
            }
        }

        return true;
    }

    public boolean getStoryFiles(ArrayList<Story> stories) throws InterruptedException, IOException
    {
        for (Story story : stories)
        {
            jsonOutput = new JsonObject();

            jsonOutput.addProperty("request", "get file");
            jsonOutput.addProperty("fileType", "story");
            jsonOutput.addProperty("username", story.getStoryPoster());
            jsonOutput.addProperty("storyID", story.getStoryID());

            dtp.sendJson(jsonOutput);

            while (jsonInput == null)
            {
                Thread.sleep(100);
            }

            if (checkJsonResult())
            {
                fileHandler.decodeFile(jsonInput.get("file").getAsString(),
                        MEDIA_PATH + story.getStoryPoster() + "/stories/" + story.getStoryID() + ".png");
            }

            else
            {
                jsonInput = null;

                return false;
            }
        }

        return true;
    }

    public ArrayList<Post> getHomePosts(String username) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //add the information to the jsonOutput
        jsonOutput.addProperty("request", "get home posts");

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            ArrayList<Post>
                    homePosts =
                    gson
                            .fromJson
                                    (jsonInput
                                                    .get
                                                            ("home posts")
                                                    .getAsString(),
                    new TypeToken
                            <List
                                    <Post>>
                            (){}.
                            getType());

            jsonInput = null;

            return homePosts;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public ArrayList<Story> getHomeStories(String username) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //add the information to the jsonOutput
        jsonOutput.addProperty("request", "get home stories");

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            ArrayList<Story> homeStories = gson.fromJson(jsonInput.get("home stories").getAsString(),
                    new TypeToken<List<Story>>(){}.getType());

            jsonInput = null;

            return homeStories;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean getProfilePicture(String username) throws InterruptedException, IOException
    {
        jsonOutput = new JsonObject();

        jsonOutput.addProperty("request", "get file");
        jsonOutput.addProperty("fileType", "profile");
        jsonOutput.addProperty("username", username);

        dtp.sendJson(jsonOutput);

        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        if (checkJsonResult())
        {
            fileHandler.decodeFile(jsonInput.get("file").getAsString(),
                    MEDIA_PATH + username + ".png");

            jsonInput = null;

            return true;
        }

        else
        {
            jsonInput = null;
            return false;
        }
    }

    public User getUser(String username) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //add the information to the jsonOutput
        jsonOutput.addProperty("request", "get user");
        jsonOutput.addProperty("username", username);

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            User user = gson.fromJson(jsonInput, User.class);

            jsonInput = null;

            return user;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public User updateUser(User user, String fullName, String password, String email, String bio) throws InterruptedException
    {
        //initialize jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "update user");
        jsonOutput.addProperty("fullName", fullName);
        jsonOutput.addProperty("password", password);
        jsonOutput.addProperty("email", email);
        jsonOutput.addProperty("bio", bio);

        //send the jsonOutput
        dtp.sendJson(jsonOutput);

        //wait for a response
        while(jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if(checkJsonResult())
        {
            //update the activeUser object
            user.setFullName(fullName);
            user.setPassword(password);
            user.setLocation(email);
            user.setBio(bio);

            jsonInput = null;

            return user;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean deleteUser(String username) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "delete user");

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);

        //wait for a response
        while(jsonInput == null)
        {
            Thread.sleep(100);
        }

        if(checkJsonResult())
        {
            //deleting files
            fileHandler.deleteDirectory(MEDIA_PATH);

            jsonInput = null;

            return true;
        }

        else
        {
            jsonInput = null;

            return false;
        }
    }

    public void logout()
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "logout");

        //send the jsonOutput to the client
        dtp.sendJson(jsonOutput);
    }

    //post

    public Post post(String imageAddress, String caption) throws InterruptedException, IOException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        String encodedFile = fileHandler.encodeFile(new File(imageAddress));

        //fill the jsonOutput
        jsonOutput.addProperty("request", "post");
        jsonOutput.addProperty("caption", caption);
        jsonOutput.addProperty("file", encodedFile);

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            //get the post object
            Post post = gson.fromJson(jsonInput.get("post").getAsString(),
                    Post.class);

            fileHandler.decodeFile(encodedFile,
                    MEDIA_PATH + post.getPoster() + "/posts/" + post.getPostID() + ".png");

            jsonInput = null;

            return post;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean deletePost(Post post) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "delete post");
        jsonOutput.addProperty("postID", post.getPostID());

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the text operation was successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    //story

    public Story story(String imageAddress) throws IOException, InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        String encodedFile = fileHandler.encodeFile(new File(imageAddress));

        //fill the jsonOutput
        jsonOutput.addProperty("request", "story");
        jsonOutput.addProperty("file", encodedFile);

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            //get the post object
            Story story = gson.fromJson(jsonInput.get("story").getAsString(),
                    Story.class);

            fileHandler.decodeFile(encodedFile,
                    MEDIA_PATH + story.getStoryPoster() + "/stories/" + story.getStoryID() + ".png");

            jsonInput = null;

            return story;
        }

        //the text operation was successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean deleteStory(Story story) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "delete story");
        jsonOutput.addProperty("storyID", story.getStoryID());

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    //message

    public Message message(String messageReceiver, String messageText) throws InterruptedException
    {
        //initialize the output json
        jsonOutput = new JsonObject();

        //add the information to the output json
        jsonOutput.addProperty("request", "send message");
        jsonOutput.addProperty("messageText", messageText);
        jsonOutput.addProperty("messageReceiver", messageReceiver);

        //send the output json to the server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while(jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if(checkJsonResult())
        {
            //get the new message instance
            Message message = gson.fromJson(jsonInput.get("message").getAsString(), Message.class);

            jsonInput = null;

            return message;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean deleteMessage(Message message) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "delete message");
        jsonOutput.addProperty("messageID", message.getMessageID());

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    //comment

    public Comment comment(Post post, String commentText) throws InterruptedException
    {
        //initialize the output json
        jsonOutput = new JsonObject();

        //add the information to the output json
        jsonOutput.addProperty("request", "comment");
        jsonOutput.addProperty("postID", post.getPostID());
        jsonOutput.addProperty("commentText", commentText);

        //send the output json to the server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while(jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if(checkJsonResult())
        {
            //get the new message instance
            Comment comment = gson.fromJson(jsonInput.get("comment").getAsString(), Comment.class);

            jsonInput = null;

            return comment;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return null;
        }
    }

    public boolean deleteComment(Comment comment) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the jsonOutput
        jsonOutput.addProperty("request", "delete comment");
        jsonOutput.addProperty("commentID", comment.getCommentID());

        //send the jsonOutput to server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    //like

    public boolean like(Post post) throws InterruptedException
    {
        //initialize the output json
        jsonOutput = new JsonObject();

        //add the information to the output json
        jsonOutput.addProperty("request", "like");
        jsonOutput.addProperty("postID", post.getPostID());
        jsonOutput.addProperty("poster", post.getPoster());

        //send the jsonOutput to the server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while(jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if(checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    public boolean unlike(Post post) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //add the information to the output json
        jsonOutput.addProperty("request", "unlike");
        jsonOutput.addProperty("postID", post.getPostID());

        //send the jsonOutput to the server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while(jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if(checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    //follow

    public boolean follow(String followed) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the output json
        jsonOutput.addProperty("request", "follow");
        jsonOutput.addProperty("followed", followed);

        //send the jsonOutput to the server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }

    public boolean unfollow(String unfollowed) throws InterruptedException
    {
        //initialize the jsonOutput
        jsonOutput = new JsonObject();

        //fill the output json
        jsonOutput.addProperty("request", "unfollow");
        jsonOutput.addProperty("unfollowed", unfollowed);

        //send the jsonOutput to the server
        dtp.sendJson(jsonOutput);

        //wait for a response
        while (jsonInput == null)
        {
            Thread.sleep(100);
        }

        //the operation was successful
        if (checkJsonResult())
        {
            jsonInput = null;

            return true;
        }

        //the operation was not successful
        else
        {
            jsonInput = null;

            return false;
        }
    }
}
