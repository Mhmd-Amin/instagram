package sbu.cs.group4.frontEnd.View.Children;


import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.User;
import sbu.cs.group4.frontEnd.View.Parent.NotificationController;

import java.io.IOException;

public class SearchController extends NotificationController
{
    @FXML
    private JFXTextField SearchField;
    @FXML
    private Text resultText;

    @FXML
    void SearchButton(ActionEvent event) throws IOException, InterruptedException
    {
        User user;
        if ((user = serverHandler.getUser(SearchField.getText())) != null)
        {
            FindProfileController.setUser(user);
            switchScene(event, "FindProfileTab");
        }

        else
        {
            resultText.setText("user not found");
        }
    }
}