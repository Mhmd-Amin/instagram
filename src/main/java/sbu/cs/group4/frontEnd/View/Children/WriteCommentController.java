package sbu.cs.group4.frontEnd.View.Children;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import sbu.cs.group4.connectors.elements.Comment;
import sbu.cs.group4.connectors.elements.Post;
import sbu.cs.group4.frontEnd.View.Parent.ControllerClass;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class WriteCommentController extends ControllerClass implements Initializable
{
    private static Post post;

    @FXML
    private JFXTextField CommentField;
    @FXML
    private ListView<String> CommentList;
    @FXML
    private Text UsernameField;


    public static void setPost(Post newPost)
    {
        post = newPost;
    }

    @FXML
    void backButton(ActionEvent event) throws IOException
    {
        switchScene(event, "HomeTab");
    }

    @FXML
    void SendButton(ActionEvent event) throws IOException, InterruptedException
    {
        Comment comment;
        if ((comment = serverHandler.comment(post, CommentField.getText())) != null)
        {
            post.addComment(comment);
            switchScene(event, "WriteComment");
        }

        else
        {
            returnToProfileTab(event);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        UsernameField.setText(activeUser.getUsername());

        List<String> comments = post.getCommentsAsString();
        CommentList.setItems(FXCollections.observableList(comments));
    }
}